<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function index(Request $request){
        return response()->json(Room::all());
    }

    public function store(Request $request){
        $room = Room::store($request);
        return response()->json($room);
    }

    public function availableRooms(Request $request){
        $rooms = Room::all();
        $availableRooms = array();
        foreach($rooms as $room){
            if(count($room->bookings)==0){
                array_push($availableRooms,$room);
            }
        }
        return response()->json($availableRooms);
    }

    public function booking(Request $request){
        $booking = Booking::store($request);
        return response()->json($booking);
    }

}
