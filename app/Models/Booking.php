<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'bookings';
    protected $primaryKey = 'id';

    public function room()
    {
        return $this->belongsTo(Room::class, 'fk_room_id','id');
    }
    public static function store($request){
        $booking = new Booking;
        $booking->description = $request->get('description');
        $booking->is_served = true;
        $booking->fk_room_id = $request->get('room_id');
        $booking->save();
        return $booking;
    }

}
