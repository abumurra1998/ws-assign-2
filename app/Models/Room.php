<?php

namespace App\Models;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $table = 'rooms';
    protected $primaryKey = 'id';


    public function bookings()
    {
        return $this->hasMany(Booking::class, 'fk_room_id','id');
    }
    public static function store($request){
        $room = new Room();
        $room->name = $request->get('name');
        $room->description = $request->get('description');
        $room->save();
        return $room;
    }
}
